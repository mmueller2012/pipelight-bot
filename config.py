BotConfig = {
	"nick"		: "[pipebot]",
	"network"	: "irc.freenode.net",
	"channels" 	: ["#pipelight-test", "#pipelight"],

	"plugins"	: {
		
		"hangman" : {
			"wordlist"	: "/home/dark/hangman.txt",
			"allowed"	: ["#pipelight-test"]
		},

		"bitbucket" : {
			"checkInterval"		: 60*10,
			"repositories" : [
				{
					"user" 		: "mmueller2012",
					"project" 	: "pipelight",
					"channels"	: ["#pipelight-test", "#pipelight"]
				},
				{
					"user" 		: "mmueller2012",
					"project" 	: "pipelight-sandbox",
					"channels"	: ["#pipelight-test", "#pipelight"]
				},
				{
					"user" 		: "mmueller2012",
					"project" 	: "pipelight-bot",
					"channels"	: ["#pipelight-test", "#pipelight"]
				},
				{
					"user"		: "mmueller2012",
					"project"	: "fds-team-website",
					"channels"	: ["#pipelight-test", "#pipelight"]
				}
			]
		},

		"github" : {
			"checkInterval"		: 60*10,
			"repositories" : [
				{
					"user" 		: "anish",
					"project" 	: "archlinux",
					"channels"	: ["#pipelight-test", "#pipelight"]
				},
				{
					"user" 		: "ryao",
					"project" 	: "pipelight-overlay",
					"channels"	: ["#pipelight-test", "#pipelight"]
				}
			]
		},

		"launchpad" : {
			"checkInterval"		: 60*10,
			"projects" : [
				{
					"name"		: "pipelight"
				},
				{
					"name"		: "netflix-desktop"
				}
			]

		}

	}
}
