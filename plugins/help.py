class PluginHelp():

	def __init__(self, bot):
		bot.commandRegister("^!help\s*$", self.help, "!help - prints the command list")
		self.bot = bot;

		print "*** Help plugin loaded"

	def help(self, msg, channel, nick, client, msgMatch):
		for command in self.bot.commands:
			if not command["hidden"]:
				self.bot.ircbot.sendMsg(channel, command["helpString"])
