import urllib
import urllib2
import re
import threading

from launchpadlib.launchpad import Launchpad
from lazr.restfulclient.errors import HTTPError
from os.path import expanduser

class PluginLaunchpad():

	def __init__(self, bot):
		self.bot = bot

		# Try to get config
		try:
			self.config = bot.config["plugins"]["launchpad"]
		except KeyError:
			print "Launchpad configuration not found, plugin will not be loaded!"
			return

		# Check for necessary values
		try:
			self.config["projects"]
		except KeyError:
			print "Launchpad: Configuration doesn't contain all necessary keys!"
			return
		
		self.lock = threading.Lock()
		self.projects = []
		for i, proj in enumerate(self.config["projects"]):
			try:
				self.projects.append({
						"name"		: proj["name"],
						"questions"	: [],
						"bugs"		: [],
						"faqs"		: []
					})
			except KeyError:
				print "Launchpad: Incomplete configuration for repository %d" % i

		bot.timerRegister(self.config["checkInterval"], self.update)
		self.updateInternal()

		bot.commandRegister("^!question (.+)$", self.searchQuestion, 	"!question query - search for question")
		bot.commandRegister("^!bug (.+)$", 		self.searchBug, 		"!bug query - search for bug")
		bot.commandRegister("^!faq (.+)$", 		self.searchFaq, 		"!faq query - search for faq")
		bot.commandRegister("#([0-9]{3,})",		self.showBugInfo)

		print "*** Launchpad plugin loaded"


	def getFAQs(self, name):
		# There is no launchpad API for faq entries :-/
		faqs 	= []
		regex 	= re.compile('<a\s+href="(?P<url>[^"]+)"\s+class="sprite\s+faq">(?P<title>[^<]+)</a>')

		try:
			website = urllib2.urlopen("https://answers.launchpad.net/%s/+faqs" % name).read()
		except (urllib2.URLError, urllib2.HTTPError, ValueError) as e:
			print "Launchpad: Failed to fetch FAQ entries for %s - %s" % (name, e.strerror)
			raise KeyError("fetch failed, update not possible")

		for match in regex.findall(website):
			faqs.append({
				"url" 	: "https://answers.launchpad.net%s" % match[0],
				"title" : match[1]
				})

		return faqs

	def updateInternal(self):
		with self.lock:
			for proj in self.projects:
				name = proj["name"]

				# Update questions and bugs
				try:
					lp = Launchpad.login_anonymously('ppadata', 'edge', expanduser("~") + '/.launchpadlib/cache/', version='devel')
					if lp:
						lpproj = lp.projects[name]
						if lpproj:
							proj["questions"] 	= lpproj.searchQuestions()
							proj["bugs"]		= lpproj.searchTasks()

				except HTTPError as e:
					print "Launchpad: Failed to get changes for %s - %s" % (proj["name"], e.strerror)

				# Update faqs
				try:
					proj["faqs"] = self.getFAQs(name)
				except KeyError:
					print "Launchpad: Failed to get FAQ entires"

	def update(self):
		self.updateInternal()

	def searchQuestion(self, msg, channel, nick, client, msgMatch):
		term = msgMatch.group(1).strip().lower()
		if len(term) < 3:
			self.bot.ircbot.sendMsg(channel, "Your search string is too short (at least 3 characters required)")
			return

		found = False

		with self.lock:
			for proj in self.projects:

				numResults = 0
				for question in proj["questions"]:
					if question.title.lower().find(term) != -1:

						if numResults < 3:
							self.bot.ircbot.sendMsg(channel, "[%s] \x0312%s \x03*\x034 %s :\x0310 %s\x03" %
														(proj["name"], question.web_link, question.status, question.title))

						numResults += 1
						found = True

				if numResults > 3:
					self.bot.ircbot.sendMsg(channel, "[%s] + %d additional entries" % (proj["name"], numResults - 3))

		if not found:
			self.bot.ircbot.sendMsg(channel, "No such question found!")

	def searchBug(self, msg, channel, nick, client, msgMatch):
		term = msgMatch.group(1).strip().lower()
		if len(term) < 3:
			self.bot.ircbot.sendMsg(channel, "Your search string is too short (at least 3 characters required)")
			return

		found = False

		with self.lock:
			for proj in self.projects:

				numResults = 0
				for bug in proj["bugs"]:
					if bug.title.lower().find(term) != -1:

						if numResults < 3:
							title_match = re.compile("^Bug #([0-9]*) in [^:]*: \"(.*)\"$").match(bug.title)
							if not title_match:
								continue

							self.bot.ircbot.sendMsg(channel, "[%s] \x0313%s \x03*\x034 %s :\x0310 %s\x03" %
														(proj["name"], bug.web_link, bug.status, "%s" % title_match.group(2) ))

						numResults += 1
						found = True

				if numResults > 3:
					self.bot.ircbot.sendMsg(channel, "[%s] + %d additional entries" % (proj["name"], numResults - 3))

		if not found:
			self.bot.ircbot.sendMsg(channel, "No such bug found!")

	def searchFaq(self, msg, channel, nick, client, msgMatch):
		term = msgMatch.group(1).strip().lower()
		if len(term) < 3:
			self.bot.ircbot.sendMsg(channel, "Your search string is too short (at least 3 characters required)")
			return

		found = False

		with self.lock:
			for proj in self.projects:

				numResults = 0
				for faq in proj["faqs"]:
					if faq["title"].lower().find(term) != -1:

						if numResults < 3:
							self.bot.ircbot.sendMsg(channel, "[%s] \x0311%s :\x0310 %s\x03" %
														(proj["name"], faq["url"], faq["title"] ))

						numResults += 1
						found = True

				if numResults > 3:
					self.bot.ircbot.sendMsg(channel, "[%s] + %d additional entries" % (proj["name"], numResults - 3))

		if not found:
			self.bot.ircbot.sendMsg(channel, "No such faq found!")

	def showBugInfo(self, msg, channel, nick, client, msgMatch):
		term = msgMatch.group(1).strip().lower()

		with self.lock:
			for proj in self.projects:

				numResults = 0
				for bug in proj["bugs"]:
					title_match = re.compile("^Bug #([0-9]*) in [^:]*: \"(.*)\"$").match(bug.title)
					if not title_match:
						continue

					if title_match.group(1) == term:
						self.bot.ircbot.sendMsg(channel, "^^^ [%s] \x0313%s \x03*\x034 %s :\x0310 %s\x03" %
													(proj["name"], bug.web_link, bug.status, "%s" % title_match.group(2) ))
						return