import urllib
import urllib2
import json
import copy

class PluginGithub():

	def __init__(self, bot):
		self.bot = bot

		# Try to get config
		try:
			self.config = bot.config["plugins"]["github"]
		except KeyError:
			print "Github configuration not found, plugin will not be loaded!"
			return

		# Check for necessary values
		try:
			self.config["repositories"]
			self.config["checkInterval"]
		except KeyError:
			print "Github: Configuration doesn't contain all necessary keys!"
			return
		
		self.repos = []
		for i, repo in enumerate(self.config["repositories"]):
			try:
				self.repos.append({
						"user"		: repo["user"], 
						"project" 	: repo["project"],
						"channels"	: repo["channels"],
						"lastTime" 	: None,
						"knownSHA"	: [],
						"changes"	: []
					})
			except KeyError:
				print "Github: Incomplete configuration for repository %d" % i

		bot.timerRegister(self.config["checkInterval"], self.update)
		self.updateInternal()

		print "*** Github plugin loaded"

	def updateInternal(self):
		for repo in self.repos:
			decodedData = None
			repoBackup 	= copy.deepcopy(repo)

			try:
				parameters	= urllib.urlencode({'since' : repo["lastTime"]}) if repo["lastTime"] else ""
				request 	= urllib2.Request(	"https://api.github.com/repos/%s/%s/commits?%s" 
												% (repo["user"], repo["project"], parameters))

				request.add_header('User-Agent', 'Pipelight-Bot (fds-team.de)')
				jsonData 	= urllib2.urlopen(request)
				decodedData = json.load(jsonData)

			except (urllib2.URLError, urllib2.HTTPError, ValueError) as e:
				print "Github: Failed to get changes for %s/%s - %s" % (repo["user"], repo["project"], e.strerror)
				continue

			if len(decodedData) <= 0:
				continue

			try:
				if repo["lastTime"]:

					for change in decodedData:
						if change["sha"] not in repo["knownSHA"]:
							repo["changes"].append({
									"sha"		: change["sha"],
									"author" 	: change["commit"]["author"]["name"],
									"message"	: change["commit"]["message"]
								})

				repo["lastTime"] = decodedData[0]["commit"]["committer"]["date"]
				repo["knownSHA"] = []

				for change in decodedData:
					if change["commit"]["committer"]["date"] == repo["lastTime"]:
						repo["knownSHA"].append(change["sha"])

			except KeyError: 
				print "Github: There is something wrong with the received data for %s/%s" % (repo["user"], repo["project"])

				# Restore old data
				for key in repoBackup:
					repo[key] = repoBackup[key]

				continue


	def announceChanges(self):
		for repo in self.repos:
			for change in repo["changes"]:
				for channel in repo["channels"]:
					self.bot.ircbot.sendMsg(	channel, "[%s] \x0312%s \x03*\x034 %s :\x0310 %s\x03" %
												(repo["project"], change["author"].strip(), change["sha"][:12], change["message"].strip()))

			# Remove all changes so that we don't announce them twice
			repo["changes"] = []


	def update(self):
		self.updateInternal()
		self.announceChanges()
