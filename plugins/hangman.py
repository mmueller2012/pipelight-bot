import threading
import random
import re
import time

class PluginHangman():

	def __init__(self, bot):

		# Try to get config
		try:
			self.config = bot.config["plugins"]["hangman"]
		except KeyError:
			print "Hangman configuration not found, plugin will not be loaded!"
			return

		# Check for necessary values
		try:
			self.config["wordlist"]
		except KeyError:
			print "Hangman: Configuration doesn't contain all necessary keys!"
			return

		# load wordlist
		if not self.loadWordlist(self.config["wordlist"]):
			print "Hangman: Couldn't load wordlist, loading plugin failed!"
			return

		bot.commandRegister("^!hangman\s*$", 	self.hangman, 	"!hangman - start a hangman game")
		bot.commandRegister("^!h (.+)$", 		self.guess, 	"!h letter/word - guess a letter / word in hangman game")
		
		self.games 	= {}
		self.bot 	= bot;
		self.lock 	= threading.Lock()

		print "*** Hangman plugin loaded"

	def loadWordlist(self, filename):
		try:
			with open(filename) as f:
				self.wordDB = f.readlines()
			return True
		except IOError:
			return False		

	def chooseWord(self):
		if len(self.wordDB) > 0:	
			tries = 10

			while tries > 0:
				tries  -= 1
				entry 	= random.randint(0, len(self.wordDB)-1)
				text 	= self.wordDB[entry].strip().lower()

				if len(text) < 3:
					continue
				if not re.match("^[A-Za-z]+$", text):
					continue

				return text

		return "pipelight"

	def prepareGame(self):
		game = {}

		game["solution"] 	= self.chooseWord()
		game["status"] 		= []

		for character in game["solution"]:
			game["status"].append({
					"letter" : character,
					"solved" : False
				})

		game["stats"] 		= {}
		game["startTime"]	= time.time()

		return game;		

	# guessInternal returns the number of points
	# and if the word was solved
	def guessInternal(self, game, guess):
		guess 	= guess.strip().lower()
		points 	= 0
		solved 	= False

		# Someones tries to solve the whole word
		if len(guess) > 1:

			# Check if the word is correct
			if game["solution"] == guess:
				solved = True

				# Count the number of solved letters
				for letter in game["status"]:
					if not letter["solved"]:
						points 				+= 1
						letter["solved"] 	= True

		# Someone tries to guess a letter
		else:

			# Assume its solved until we find an unsolved letter
			solved = True

			for letter in game["status"]:
				if letter["letter"] == guess and not letter["solved"]:
					letter["solved"] = True
					points += 1
				if not letter["solved"]:
					solved = False
			
		return points, solved

	def formatHangman(self, game, highlight = None):
		resultStr = "\x03"

		for letter in game["status"]:
			if letter["solved"]:
				if highlight and letter["letter"] == highlight:
					resultStr += "\x034" + letter["letter"] + "\x03 "
				else:
					resultStr += letter["letter"] + " "
			else:
				resultStr += "_ "

		return resultStr

	def hangman(self, msg, channel, nick, client, msgMatch):
		if "allowed" in self.config:
			if channel not in self.config["allowed"]:
				self.bot.ircbot.sendMsg(channel, "This plugin is only allowed in: %s" % ", ".join(self.config["allowed"]))
				return

		with self.lock:
			answer = ""

			if channel in self.games and self.games[channel]:
				answer = "There is already a running game: "
			else:
				answer = "Starting new game: "
				self.games[channel] = self.prepareGame()

			answer += self.formatHangman(self.games[channel])

			self.bot.ircbot.sendMsg(channel, answer)

	def guess(self, msg, channel, nick, client, msgMatch):
		with self.lock:

			guess = msgMatch.group(1).strip().lower()

			if channel not in self.games or not self.games[channel]:
				self.bot.ircbot.sendMsg(channel, "There is currently no running game! Type !hangman to start one.")
				return

			game = self.games[channel]
			points, solved = self.guessInternal(game, guess)
			
			# Nothing guessed and not solved yet
			if points == 0 and not solved:
				return

			# Add points
			if nick not in game["stats"]:
				game["stats"][nick] = 0
			game["stats"][nick] += points

			if solved:
				bestPoints, bestNick = 0, ""
				for statNick, statPoints in game["stats"].iteritems():
					if statPoints > bestPoints:
						bestPoints, bestNick = statPoints, statNick

				self.bot.ircbot.sendMsg(	channel, "Solved by %s: %s - Winner: %s - %d points - duration of the game: %s seconds" % 
											(nick, game["solution"], bestNick, bestPoints, str(time.time() - game["startTime"])))

				self.games[channel] = None
				return
			
			if points > 0:
				self.bot.ircbot.sendMsg(channel, "%s(%s +%d)" % (self.formatHangman(game, guess), nick, points))
	



				