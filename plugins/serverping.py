class PluginServerPing():

	def __init__(self, bot):
		self.bot = bot
		bot.timerRegister(3*60, self.ping)

		print "*** ServerPing plugin loaded"

	def ping(self):
		self.bot.ircbot.sendPing()
