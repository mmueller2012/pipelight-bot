import urllib2
import json

class PluginBitbucket():

	def __init__(self, bot):
		self.bot = bot

		# Try to get config
		try:
			self.config = bot.config["plugins"]["bitbucket"]
		except KeyError:
			print "Bitbucket configuration not found, plugin will not be loaded!"
			return

		# Check for necessary values
		try:
			self.config["repositories"]
			self.config["checkInterval"]
		except KeyError:
			print "Bitbucket: Configuration doesn't contain all necessary keys!"
			return

		self.repos = []
		for i, repo in enumerate(self.config["repositories"]):
			try:
				self.repos.append({
						"user"		: repo["user"], 
						"project" 	: repo["project"],
						"channels"	: repo["channels"],
						"count" 	: None,
						"changes"	: []
					})
			except KeyError:
				print "Bitbucket: Incomplete configuration for repository %d" % i


		bot.timerRegister(self.config["checkInterval"], self.update)
		self.updateInternal()

		print "*** Bitbucket plugin loaded"

	def fetchChanges(self, repo, limit, startNode):
			decodedData = None

			try:
				jsonData 	= urllib2.urlopen(	"https://api.bitbucket.org/1.0/repositories/%s/%s/changesets?limit=%d&start=%s" 
												% (repo["user"], repo["project"], limit, startNode))
				decodedData = json.load(jsonData)

			except (urllib2.URLError, urllib2.HTTPError, ValueError) as e:
				print "Bitbucket: Failed to fetch changes for %s/%s - %s" % (repo["user"], repo["project"], e.strerror)
				raise KeyError("fetch failed, update not possible")

			for change in decodedData["changesets"]:
				if "author" in change and "node" in change and "message" in change:
					yield change
				else:
					print "Bitbucket: Ignoring change since it's missing some fields for %s/%s" % (repo["user"], repo["project"])

	def updateInternal(self):
		for repo in self.repos:
			decodedData = None

			try:
				jsonData 	= urllib2.urlopen("https://api.bitbucket.org/1.0/repositories/%s/%s/changesets?limit=1" % (repo["user"], repo["project"]))
				decodedData = json.load(jsonData)

			except (urllib2.URLError, urllib2.HTTPError, ValueError) as e:
				print "Bitbucket: Failed to get changes for repository %s/%s - %s" % (repo["user"], repo["project"], e.strerror)
				continue

			try:

				# Check that these values really exist
				decodedData["count"]
				decodedData["changesets"]

				if not repo["count"]:
					repo["count"] = decodedData["count"]
					continue

				if decodedData["count"] <= repo["count"]:
					continue
				
				newCommits 	= decodedData["count"] - repo["count"]
				startNode 	= decodedData["changesets"][0]["node"]
				for change in self.fetchChanges(repo, newCommits, startNode):
					repo["changes"].append(change)

				# Update this only if we successful got all the changes
				repo["count"] = decodedData["count"]

			except KeyError:
				print "Bitbucket: There is something wrong with the received data for %s/%s" % (repo["user"], repo["project"])
				continue


	def announceChanges(self):
		for repo in self.repos:
			for change in repo["changes"]:
				for channel in repo["channels"]:
					self.bot.ircbot.sendMsg(	channel, "[%s] \x0312%s \x03*\x034 %s :\x0310 %s\x03" %
												(repo["project"], change["author"].strip(), change["node"], change["message"].strip()))

			# Remove all changes so that we don't announce them twice
			repo["changes"] = []


	def update(self):
		self.updateInternal()
		self.announceChanges()
