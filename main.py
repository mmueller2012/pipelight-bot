#!/usr/bin/python

# Config
import config

# IRC Bot Class
import pythonircbot

import time

# Plugins
import plugins.help
import plugins.serverping
import plugins.bitbucket
import plugins.github
import plugins.launchpad
import plugins.hangman

class PipelightBot:

	def __init__(self):
		self.config     = None
		self.commands 	= []
		self.timers 	= []
		self.ircbot		= None

	def commandRegister(self, regEx, function, helpString = None, hidden = False):
		if not helpString:
			hidden = True

		self.commands.append({
				"regEx" 		: regEx,
				"function" 		: function,
				"helpString" 	: helpString,
				"hidden"		: hidden
			})

	def timerRegister(self, interval, function):
		self.timers.append({
				"interval" 		: interval,
				"function"		: function,
				"lastExecute"	: None
			})

	def run(self):

		# Check if config exists
		try:
			self.config = config.BotConfig
		except AttributeError:
			print "No bot configuration found! Aborting..."
			return

		# Check for necessary values
		try:
			self.config["nick"]
			self.config["network"]
			self.config["channels"]
		except KeyError:
			print "Configuration doesn't contain all necessary keys! Aborting..."
			return

		# Register Plugins
		plugins.help.PluginHelp(self)
		plugins.serverping.PluginServerPing(self)
		plugins.bitbucket.PluginBitbucket(self)
		plugins.github.PluginGithub(self)
		plugins.launchpad.PluginLaunchpad(self)
		plugins.hangman.PluginHangman(self)
		#plugins.adventure.PluginAdventure(self)
		#plugins.timesayer.PluginTimeSayer(self)

		# Create IRC Bot object
		self.ircbot = pythonircbot.Bot(self.config["nick"])

		# Register message handlers
		for command in self.commands:
			self.ircbot.addMsgHandler(command["function"], command["regEx"])
	
		# Connect to IRC network & join channels
		self.ircbot.connect(self.config["network"], sleepTime=0.8)

		for channel in self.config["channels"]:
			self.ircbot.joinChannel(channel)

		# Reset timers
		currentTime = time.time()
		for timer in self.timers:
			timer["lastExecute"] = currentTime

		while not self.ircbot.waitForDisconnect(.1):

			currentTime = time.time()
			for timer in self.timers:
				if currentTime - timer["lastExecute"] > timer["interval"]:
					timer["function"]()
					timer["lastExecute"] = currentTime


if __name__ == "__main__":
	bot = PipelightBot()
	bot.run()